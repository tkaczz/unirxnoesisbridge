﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using UniRx;

namespace UniRxNoesisBridge {
    /// <summary>
    /// 0 gc version of this, would require using reflection. So creating objects is imo faster than that.
    /// <para>Research is needed on this topic... complex one...</para>
    /// </summary>
    public class BindableReactiveCollection<T> : ReactiveCollection<T>, INotifyCollectionChanged, INotifyPropertyChanged {
        private PropertyChangedEventArgs countEventArgs = new PropertyChangedEventArgs("Count");
        private PropertyChangedEventArgs indexerEventArgs = new PropertyChangedEventArgs("Item[]");

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyChangedEventArgs eventArgs) {
            PropertyChanged?.Invoke(this, eventArgs);
        }

        private void OnCollectionChanged(NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs) {
            CollectionChanged?.Invoke(this, notifyCollectionChangedEventArgs);
        }

        protected override void ClearItems() {
            base.ClearItems();

            PropertyChanged?.Invoke(this, countEventArgs);
            PropertyChanged?.Invoke(this, indexerEventArgs);

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);

            this.countEventArgs = null;
            this.indexerEventArgs = null;
        }

        protected override void InsertItem(int index, T item) {
            base.InsertItem(index, item);

            OnPropertyChanged(countEventArgs);
            OnPropertyChanged(indexerEventArgs);

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        protected override void MoveItem(int oldIndex, int newIndex) {
            base.MoveItem(oldIndex, newIndex);

            OnPropertyChanged(indexerEventArgs);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, this[newIndex], newIndex, oldIndex));
        }

        protected override void RemoveItem(int index) {
            T item = this[index];

            base.RemoveItem(index);

            OnPropertyChanged(countEventArgs);
            OnPropertyChanged(indexerEventArgs);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
        }

        protected override void SetItem(int index, T item) {
            T oldItem = this[index];

            base.SetItem(index, item);

            OnPropertyChanged(indexerEventArgs);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, oldItem, index));
        }

        public BindableReactiveCollection(string name) : base() {
            this.countEventArgs = new PropertyChangedEventArgs(name);
        }

        public BindableReactiveCollection(IEnumerable<T> collection, string name) : base(collection) {
            this.countEventArgs = new PropertyChangedEventArgs(name);
        }

        public BindableReactiveCollection(List<T> list, string name) : base(list) {
            this.countEventArgs = new PropertyChangedEventArgs(name);
        }
    }
}