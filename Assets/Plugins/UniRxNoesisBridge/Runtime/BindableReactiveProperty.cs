﻿using System.ComponentModel;
using UniRx;

namespace UniRxNoesisBridge {
    /// <summary>
    /// Two-way, source <-> target, bindable reactive property
    /// </summary>
    public class BindableReactiveProperty<T> : ReactiveProperty<T>, INotifyPropertyChanged {
        private PropertyChangedEventArgs propertyChangedEventArgs;

        public event PropertyChangedEventHandler PropertyChanged;

        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
            this.propertyChangedEventArgs = null;
        }

        protected override void SetValue(T value) {
            base.SetValue(value);
            PropertyChanged?.Invoke(this, this.propertyChangedEventArgs);
        }

        public BindableReactiveProperty(string name) : base() {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
        }

        public BindableReactiveProperty(T initialValue, string name) : base(initialValue) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
        }
    }
}