﻿using System;
using System.ComponentModel;
using UniRx;

namespace UniRxNoesisBridge {
    /// <summary>
    /// One-way, from source to target, reactive property
    /// </summary>
    public class BindableReadOnlyReactiveProperty<T> : ReadOnlyReactiveProperty<T>, INotifyPropertyChanged {
        private IDisposable notifySub;
        private PropertyChangedEventArgs propertyChangedEventArgs;

        public event PropertyChangedEventHandler PropertyChanged;

        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
            this.notifySub.Dispose();
            this.propertyChangedEventArgs = null;
        }

        public BindableReadOnlyReactiveProperty(
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source,
#else
            UniRx.IObservable<T> source,
#endif  
            string name)
                : base(source) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
            this.notifySub = this.Subscribe(_ => PropertyChanged?.Invoke(this, this.propertyChangedEventArgs));
        }

        public BindableReadOnlyReactiveProperty(
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source,
#else
            UniRx.IObservable<T> source,
#endif  
            bool distinctUntilChanged, string name)
                : base(source, distinctUntilChanged) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
            this.notifySub = this.Subscribe(_ => PropertyChanged?.Invoke(this, this.propertyChangedEventArgs));
        }

        public BindableReadOnlyReactiveProperty(
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source,
#else
            UniRx.IObservable<T> source,
#endif  
            T initialValue, string name)
            : base(source, initialValue) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
            this.notifySub = this.Subscribe(_ => PropertyChanged?.Invoke(this, this.propertyChangedEventArgs));
        }

        public BindableReadOnlyReactiveProperty(
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source,
#else
            UniRx.IObservable<T> source,
#endif  
            T initialValue, bool distinctUntilChanged, string name)
                : base(source, initialValue, distinctUntilChanged) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
            this.notifySub = this.Subscribe(_ => PropertyChanged?.Invoke(this, this.propertyChangedEventArgs));
        }
    }
}