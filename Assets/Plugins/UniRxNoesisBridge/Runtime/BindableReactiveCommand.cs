﻿using System;
using System.Windows.Input;
using UniRx;

namespace UniRxNoesisBridge {
    public class BindableReactiveCommand : BindableReactiveCommand<Unit> {
        public BindableReactiveCommand() { }

        public BindableReactiveCommand(
#if UNITY_5_3_OR_NEWER
            System.IObservable<bool> canExecuteSource,
#else
UniRx.IObservable<bool> canExecuteSource,
#endif            
            bool initialValue = true
        )
            : base(canExecuteSource, initialValue) { }
    }

    public class BindableReactiveCommand<T> : ReactiveCommand<T>, ICommand {
        private bool disposed = false;
        private IDisposable canExecuteSubscription;

        public event EventHandler CanExecuteChanged;

        new public bool CanExecute(object parameter) {
            return base.CanExecute.Value;
        }

        public void Execute(object parameter) {
            if (parameter == null) {
                base.Execute(default);
            }
            else {
                base.Execute((T)parameter);
            }
        }

        new public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposed) {
                return;
            }

            if (disposing) {
                base.Dispose();
                this.canExecuteSubscription.Dispose();
            }

            disposed = true;
        }

        public BindableReactiveCommand() {
            this.canExecuteSubscription = Disposable.Empty;
        }

        public BindableReactiveCommand(
#if UNITY_5_3_OR_NEWER
            System.IObservable<bool> canExecuteSource,
#else
            UniRx.IObservable<bool> canExecuteSource,
#endif        
            bool initialValue = true)
                : base(canExecuteSource, initialValue) {
            this.canExecuteSubscription = base.CanExecute
                .Subscribe(_ => CanExecuteChanged?.Invoke(this, EventArgs.Empty));
        }
    }
}