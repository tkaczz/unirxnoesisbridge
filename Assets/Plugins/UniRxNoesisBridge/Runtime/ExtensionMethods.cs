﻿using System.Collections.Generic;
using UniRx;

namespace UniRxNoesisBridge {
    public static class BindableReactiveCommandExtensions {
        /// <summary>
        /// Create non parameter commands. CanExecute is changed from canExecute sequence.
        /// </summary>
        public static BindableReactiveCommand ToBindableReactiveCommand(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<bool> canExecuteSource,
#else
            UniRx.IObservable<bool> canExecuteSource,
#endif
            bool initialValue = true) {
            return new BindableReactiveCommand(canExecuteSource, initialValue);
        }

        /// <summary>
        /// Create parametered comamnds. CanExecute is changed from canExecute sequence.
        /// </summary>
        public static BindableReactiveCommand<T> ToBindableReactiveCommand<T>(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<bool> canExecuteSource,
#else
            UniRx.IObservable<bool> canExecuteSource,
#endif
            bool initialValue = true) {
            return new BindableReactiveCommand<T>(canExecuteSource, initialValue);
        }
    }

    public static class BindableReactivePropertyExtensions {
        public static IReadOnlyReactiveProperty<T> ToBindableReactiveProperty<T>(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source
#else
            UniRx.IObservable<T> source
#endif
        ) {
            return new BindableReadOnlyReactiveProperty<T>(source, nameof(source));
        }

        public static IReadOnlyReactiveProperty<T> ToBindableReactiveProperty<T>(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source,
#else
            UniRx.IObservable<T> source,
#endif
            T initialValue
        ) {
            return new BindableReadOnlyReactiveProperty<T>(source, initialValue, nameof(source));
        }

        public static BindableReadOnlyReactiveProperty<T> ToReadOnlyBindableReactiveProperty<T>(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source
#else
            UniRx.IObservable<T> source
#endif
        ) {
            return new BindableReadOnlyReactiveProperty<T>(source, nameof(source));
        }

        /// <summary>
        /// Create ReadOnlyReactiveProperty with distinctUntilChanged: false.
        /// </summary>
        public static BindableReadOnlyReactiveProperty<T> ToSequentialReadOnlyBindableReactiveProperty<T>(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source
#else
            UniRx.IObservable<T> source
#endif
        ) {
            return new BindableReadOnlyReactiveProperty<T>(source, distinctUntilChanged: false, nameof(source));
        }

        public static BindableReadOnlyReactiveProperty<T> ToReadOnlyBindableReactiveProperty<T>(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source,
#else
            UniRx.IObservable<T> source,
#endif
            T initialValue
        ) {
            return new BindableReadOnlyReactiveProperty<T>(source, initialValue, nameof(source));
        }

        /// <summary>
        /// Create ReadOnlyReactiveProperty with distinctUntilChanged: false.
        /// </summary>
        public static BindableReadOnlyReactiveProperty<T> ToSequentialReadOnlyBindableReactiveProperty<T>(this
#if UNITY_5_3_OR_NEWER
            System.IObservable<T> source,
#else
            UniRx.IObservable<T> source,
#endif
            T initialValue
        ) {
            return new BindableReadOnlyReactiveProperty<T>(source, initialValue, distinctUntilChanged: false, nameof(source));
        }

        //not needed
        //public static IObservable<T> SkipLatestValueOnSubscribe<T>(this IReadOnlyReactiveProperty<T> source) {
        //    return source.HasValue ? source.Skip(1) : source;
        //}
    }

    public static class BindableReactiveCollectionExtensions {
        public static BindableReactiveCollection<T> ToBindableReactiveCollection<T>(this IEnumerable<T> source) {
            return new BindableReactiveCollection<T>(source, nameof(source));
        }
    }
}