﻿using NUnit.Framework;
using UniRx;
using UniRxNoesisBridge;

namespace Tests {
    [TestFixture]
    public class ReactiveCollectionIndexTests {
        [Test]
        public void CheckForAddedIndex() {
            int result = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.CollectionChanged += (sender, eventArgs) => result = eventArgs.NewStartingIndex;
            collection.Add(1);
            collection.Add(2);

            Assert.IsTrue(result == 1);
        }

        [Test]
        public void CheckForRemovedIndex() {
            int result = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.CollectionChanged += (sender, eventArgs) => result = eventArgs.OldStartingIndex;

            collection.Add(1);
            collection.Add(2);

            collection.RemoveAt(1);

            Assert.IsTrue(result == 1);
        }

        [Test]
        public void CheckForMovedIndex() {
            int oldIndex = 100;
            int newIndex = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.CollectionChanged += (sender, eventArgs) => {
                oldIndex = eventArgs.OldStartingIndex;
                newIndex = eventArgs.NewStartingIndex;
            };

            collection.Add(1);
            collection.Add(2);

            collection.Move(0, 1);

            Assert.IsTrue(oldIndex == 0 && newIndex == 1);
        }

        [Test]
        public void CheckForSetIndex() {
            int newIndex = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.CollectionChanged += (sender, eventArgs) => newIndex = eventArgs.NewStartingIndex;

            collection.Add(1);
            collection.Add(2);

            collection[1] = 0;

            Assert.IsTrue(newIndex == 1);
        }
    }

    [TestFixture]
    public class ReactiveCollectionValueTests {
        [Test]
        public void CheckForAddedItem() {
            int result = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.CollectionChanged += (sender, eventArgs) => result = (int)eventArgs.NewItems[0];
            collection.Add(1);

            Assert.IsTrue(result == 1);
        }

        [Test]
        public void CheckForRemovedItem() {
            int result = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.Add(2);
            collection.Add(3);

            collection.CollectionChanged += (sender, eventArgs) => result = (int)eventArgs?.OldItems[0];

            collection.RemoveAt(1);

            Assert.IsTrue(result == 3);
        }

        [Test]
        public void CheckForMovedItem() {
            int oldIndex = 100;
            int newIndex = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.Add(2);
            collection.Add(3);

            collection.CollectionChanged += (sender, eventArgs) => {
                oldIndex = (int)eventArgs?.OldStartingIndex;
                newIndex = (int)eventArgs?.NewStartingIndex;
            };

            collection.Move(1, 0);

            Assert.IsTrue(oldIndex == 1 && newIndex == 0);
        }

        [Test]
        public void CheckForReplacedItem() {
            int newItem = 100;
            int oldItem = 100;
            var collection = new BindableReactiveCollection<int>("collection");

            collection.Add(2);
            collection.Add(3);

            collection.CollectionChanged += (sender, eventArgs) => {
                oldItem = (int)eventArgs?.OldItems[0];
                newItem = (int)eventArgs?.NewItems[0];
            };

            collection[0] = 20;

            Assert.IsTrue(newItem == 20 && oldItem == 2);
        }

        [Test]
        public void CheckForEventIfCasted() {
            ReactiveCollection<int> col = new BindableReactiveCollection<int>("col");

            bool result = false;

            BindableReactiveCollection<int> casted = (BindableReactiveCollection<int>)col;

            casted.CollectionChanged += (sender, args) => result = true;

            col.Add(2);

            Assert.IsTrue(result);
        }
    }
}