﻿using NUnit.Framework;
using UniRxNoesisBridge;

namespace Tests {
    [TestFixture]
    public class BindableReactivePropertyTests {
        [Test]
        public void CheckForUnitialized() {
            int result = 0;
            var bdbReactProp = new BindableReactiveProperty<int>("reactProp");

            bdbReactProp.PropertyChanged += (sender, args) => {
                result = bdbReactProp.Value;
            };

            bdbReactProp.Value = 2;

            Assert.IsTrue(result == 2);
        }

        [Test]
        public void CheckForInitialized() {
            int result = 2;
            var bdbReactProp = new BindableReactiveProperty<int>(result, "reactProp");

            bdbReactProp.PropertyChanged += (sender, args) => {
                result = bdbReactProp.Value;
            };

            bdbReactProp.Value = 2;

            Assert.IsTrue(result == 2);
        }
    }
}