﻿using NUnit.Framework;
using UniRx;
using UniRxNoesisBridge;

namespace Tests {
    [TestFixture]
    public class BindableReadOnlyReactivePropertyTests {
        [Test]
        public void CheckForUnitialized() {
            int result = 0;
            var reactProp = new ReactiveProperty<int>();
            var roReactProp = new BindableReadOnlyReactiveProperty<int>(reactProp, "roReactProp");

            roReactProp.PropertyChanged += (sender, args) => {
                result = roReactProp.Value;
            };

            reactProp.Value = 2;

            Assert.IsTrue(result == 2);
        }

        [Test]
        public void CheckForInitialized() {
            int result = 2;
            var reactProp = new ReactiveProperty<int>(result);
            var roReactProp = new BindableReadOnlyReactiveProperty<int>(reactProp, "roReactProp");

            roReactProp.PropertyChanged += (sender, args) => {
                result = roReactProp.Value;
            };

            reactProp.Value = 3;

            Assert.IsTrue(result == 3);
        }
    }
}