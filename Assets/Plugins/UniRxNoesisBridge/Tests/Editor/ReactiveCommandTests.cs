﻿using NUnit.Framework;
using UniRx;
using UniRxNoesisBridge;

namespace Tests {
    [TestFixture]
    public class ReactiveCommandTests {
        [Test]
        public void CheckIfNonTypedNoCtorReturnTrueCanExecute() {
            var command = new BindableReactiveCommand();

            Assert.IsTrue(command.CanExecute(null));
        }

        [Test]
        public void CheckIfFalseCanExecuteReturnsFalse() {
            var boolProp = new ReactiveProperty<bool>(false);
            var command = new BindableReactiveCommand(boolProp);

            Assert.IsFalse(command.CanExecute(null));
        }
    }
}